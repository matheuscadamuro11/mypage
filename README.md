# Introdução

## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação - Introdução a Engenharia. 

|Nome| gitlab user|
|---|---|
|Matheus D. Cadamuro|@matheuscadamuro11|
|Tais M. Riquinho|@taismanicariquinho|
|Luiz A. Silva dos Santos|@luizalexandre212|

# Documentação

A documentação do projeto pode ser acessada pelo link:

https://matheuscadamuro11.gitlab.io/mypage/